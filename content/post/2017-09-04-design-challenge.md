---
date: 2017-09-04
title: "Design Challenge"
---

> Vandaag zijn we begonnen met het verzinnen van een spel. Wij willen namelijk iets doen met een app dat leuk is voor 1e jaars studenten. Wij willen daarvoor al onze ideeën bij elkaar brengen.

Brainstorm

> We zijn met elkaar gaan brainstormen over wat aantrekkelijk is voor studenten en wat hun ook leuk lijkt. We hebben verschillende schetsen en aantekingen gemaakt. We hebben nu ook al een naam bedacht voor onze app genaamd: Connect. 

Plan van aanpak
> Iedereen heeft nu al zijn eigen taak en is er ook mee bezig. We hebben alles goed verdeeld zodat we makkelijker kunnen werken.

